package calculator;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class testCalculator {
	main mainController = new main();

	@Test
	void testSuma() {
		
		int result = main.addition(1,5);
		assertEquals(result,6);
	}
	
	@Test
	void testResta() {
		
		int result = main.substraction(13, 3);
		assertEquals(result, 10);
	}
	
	@Test
	void testIsPrime() {
		
		boolean prime = main.prime(7);
		assertTrue(prime);
	}
	
	@Test
	void testIsNotPrime() {

		boolean prime = main.prime(8);
		assertFalse(prime);
	}
	
	@Test
	void testIsNon() {
	
		boolean non = main.non(6);
		assertTrue(non);
	}
	
	@Test
	void testIsNotNon() {
		
		boolean non = main.non(9);
		assertFalse(non);
	}
	
	@Test
	void testFactorial() {
		
		int fact = main.fact(6);
		assertEquals(fact,720);
	}

}
